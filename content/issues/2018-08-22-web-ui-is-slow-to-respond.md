+++
title        = "UI is slow to respond or will not load"
date         = "2018-08-22T16:35:00Z"
resolved     = true
resolvedWhen = "2018-08-22T17:30:00Z"
severity     = "disrupted"
affected     = ["Web"]
section      = "issue"
+++

*Resolved* -
The issue has been resolved. The situation will be monitored, but a regression
is not expected.
{{< track "2018-08-22 17:30:00-00:00" >}}

*Investigating* -
The incident is ongoing and another fix is being investigated. Next update in
30 minutes.
{{< track "2018-08-22 17:00:00-00:00" >}}

*Investigating* -
A fix has been identified and is being tested. Next update in 20 minutes. 
{{< track "2018-08-22 16:41:00-00:00" >}}

*Investigating* -
A third party service is causing the web interface to fail. A fix is in
progress. Next communication will be in 30 minutes.
{{< track "2018-08-22 16:35:00-00:00" >}}
